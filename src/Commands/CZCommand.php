<?php

namespace ShandiaLamp\DD\Commands;

use Inhere\Console\Command;
use Inhere\Console\Util\Interact;

class CZCommand extends Command
{
    protected static $name = 'cz';

    protected static $description = 'cz配置';

    public function execute($input, $output)
    {
        $currentDir = getcwd();
        if (file_exists($currentDir . '/.cz-config.js')) {
            $result = Interact::confirm('> 当前目录已经存在[.cz-config.js]文件，是否覆盖?', false);
            if (!$result) {
                $output->write('退出');
                return false;
            }
        }
        $content = file_get_contents(__DIR__ . '/../../templates/.cz-config.js');
        file_put_contents($currentDir . '/.cz-config.js', $content);
        $output->success('生成[.cz-config.js]成功');
    }
}
