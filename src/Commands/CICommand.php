<?php

namespace ShandiaLamp\DD\Commands;

use Inhere\Console\Command;
use Inhere\Console\Util\Interact;

class CICommand extends Command
{
    protected static $name = 'ci';

    protected static $description = 'ci配置';

    public function execute($input, $output)
    {
        $args = $input->getArgs();

        if (!$args || count($args) == 0) {
            $output->error('请指定CI配置！');
            return false;
        }
        $type = $args[0];

        if (!method_exists($this, $type)) {
            $output->error("不存在[{$type}]CI配置！");
            return false;
        }
        $this->$type($input, $output);
    }

    protected function prefix($output)
    {
        $currentDir = getcwd();
        if (file_exists($currentDir . '/.gitlab-ci.yml')) {
            $result = Interact::confirm('> 当前目录已经存在[.gitlab-ci.yml]文件，是否覆盖?', false);
            if (!$result) {
                $output->write('退出');
                return false;
            }
        }
        return true;
    }

    protected function buildFile($output, $content)
    {
        file_put_contents(getcwd() . '/.gitlab-ci.yml', $content);
        $output->success('生成[.gitlab-ci.yml]成功');
    }

    protected function golang($input, $output)
    {
        if (!$this->prefix($output)) {
            return false;
        }
        
        $lint = Interact::confirm('> 是否要校验代码风格?', true);
        $test = Interact::confirm('> 是否要进行单元测试?', true);
        $content = <<<EOF
stages:
    - test
EOF;

        $content .= <<<EOF


test:
    image: golang:1.13
    script:
        - go get -u golang.org/x/lint/golint
EOF;

        if ($lint) {
            $content .= <<<EOF

        - golint
EOF;
        }

        if ($test) {
            $content .= <<<EOF

        - go test
EOF;
        }

        $this->buildFile($output, $content);
    }

    protected function vue($input, $output)
    {
        if (!$this->prefix($output)) {
            return false;
        }
        
        $lint = Interact::confirm('> 是否要校验代码风格?', true);
        $unitTest = Interact::confirm('> 是否要进行单元测试?', true);
        $e2eTest = Interact::confirm('> 是否要进行e2e测试?', false);
        $deploy = Interact::confirm('> 是否要发布到npm?', false);

        $content = <<<EOF
stages:
    - test
EOF;

        if ($deploy) {
            $content .= <<<EOF

    - deploy
EOF;
        }

        $content .= <<<EOF


test:
    image: node
    script:
        - apt update -y && apt install -y -q xvfb libgtk2.0-0 libxtst6 libxss1 libgconf-2-4 libnss3 libasound2 libatk-bridge2.0-0 libgtk-3-0
        - npm install
EOF;

        if ($lint) {
            $content .= <<<EOF

        - npm run lint
EOF;
        }

        if ($unitTest) {
            $content .= <<<EOF

        - npm run test:unit
EOF;
        }

        if ($e2eTest) {
            $content .= <<<EOF

        - npm run test:e2e
EOF;
        }

        if ($deploy) {
            $content .= <<<EOF


deploy:
    image: node:13-alpine
    only:
        - tags
    script: 
        - echo '//registry.npmjs.org/:_authToken=\${NPM_TOKEN}' >> .npmrc
        - npm publish
EOF;
        }

        $this->buildFile($output, $content);
    }
}
